<?php


Route::post('users/register', 'Auth\RegisterController@register');
Route::any('users/login', 'Auth\LoginController@login');

Route::group(['middleware' => 'auth:api'], function() {

});

Route::group(['middleware' => 'auth:token'], function(){
    // AUTHENTICATED routes here,
    Route::get('users/index', 'TokenController@index');
    Route::any('logout', 'TokenController@logout');
    Route::any('users/logout_other_sessions', 'TokenController@logoutOthers');
});