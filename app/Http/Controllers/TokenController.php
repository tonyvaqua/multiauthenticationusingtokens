<?php

namespace App\Http\Controllers;

use App\Token;
use Illuminate\Http\Request;
use DB;
use Auth;

class TokenController extends Controller
{
    public function index(){

        $tokens = Token::whereIn('id', function($query){
            $query->select(DB::raw("max(id)"))
                ->from('tokens')
                ->groupBy('user_id');
        })->get();

        return response()->json(['data' => $tokens->toArray()], 201);

    }

    public function logout(Request $request)
    {
        $user = Auth::guard('token')->user();

        Token::where('user_id', $user->id)->where('access_token', $request->input('access_token'))->delete();

        return response()->json(['data' => 'User logged out'], 200);
    }

    public function logoutOthers(Request $request)
    {
        $user = Auth::guard('token')->user();

        Token::where('user_id', $user->id)->where('access_token', '<>', $request->input('access_token'))->delete();

        return response()->json(['data' => 'User logged out from other sessions.'], 200);
    }
}
